const Providers = require('../models/provider.model.js');

// Fetch provider with the provided id in routes
exports.findOne = (req, res) => {
    Providers.findOne({"id":req.params.id},{ "name": 1, "partners": 1 })
    .then(provider => {
        if(!provider) {
            return res.status(404).send({
                message: "The provider with id " + req.params.id + " doesn't exist" 
            });            
        }else{
            return res.status(200).send({
                data: provider
            });
           
        }
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "The provider with id " + req.params.id +   " doesn't exist" 
            });                
        }
        return res.status(500).send({
            message: "Error retrieving provider " + req.params.id
        });
    });
};