const fs = require('fs')
const mongodb = require("mongodb").MongoClient;
const readline = require('readline')
//connect to a mongodb database instance for cleaned data storage
let url = "mongodb://localhost:27017/";

const readInterface = readline.createInterface({
    input: fs.createReadStream('ads.txt'),
    console: false
});

readInterface.on('line', (line) => {
    let dataArray = []
    cleanData = line.split(" ")
    dataArray.push(cleanData)
    testData = JSON.stringify(dataArray[0])

    // console.log(JSON.parse(testData))

    mongodb.connect(
        url,
        { useNewUrlParser: true, useUnifiedTopology: true },
        (err, client) => {
          if (err) throw err;
    
          client
            .db("ads_db")
            .collection("partner")
            .insertMany(dataArray[0][1], (err, res) => {
              if (err) throw err;
    
              console.log(`Inserted: ${res.insertedCount} rows`);
              client.close();
            });
        }
      );
});
