// Define the data schema for the fields to be included in the database.
const mongoose = require("mongoose");

const Ads = mongoose.model(
  "Ads",
  new mongoose.Schema({
    title: String,
    owner: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Provider"
      }
    ]
  })
);

module.exports = Ads;