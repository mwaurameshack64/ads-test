// Define the data schema for the fields to be included in the database.
const mongoose = require("mongoose");

const Partner = mongoose.model(
  "Partner",
  new mongoose.Schema({
    title: String,
    providers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Provider"
      }
    ],
    ads: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Ads"
        }
    ]

  })
);

module.exports = Partner;