// Define the data schema for the fields to be included in the database.
const Provider = mongoose.model(
  "Provider",
  new mongoose.Schema({
    id: String,
    name: String,
    partners: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Partner"
      }
    ]
  })
);

module.exports = Provider;