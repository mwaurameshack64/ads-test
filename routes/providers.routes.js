module.exports = (app) => {
    const providers = require('../controllers/providers.controller.js');

    // Retrieve the provider details with a predefined  db param
    app.get('/providers/:id', providers.findOne);
}